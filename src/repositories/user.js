import { apiGet, apiPost, apiPut, apiDelete } from 'vue-ldc-axios/api';

export const information = async () => {
    return await apiGet('/v1/admin/profile', {
        dashboard: true
    });
}