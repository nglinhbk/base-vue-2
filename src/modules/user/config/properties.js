export const urlImage = (image) => {
    if(image.slice(0, 1) == '/'){
        image = image.slice(1);
    }
    return process.env.VUE_APP_BASE_ASSETS + image;
}