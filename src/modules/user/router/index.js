import { withPrefixRouter } from '@/config/global';

export default [
    {
        path: '/test',
        component: () => import(/* webpackChunkName: "information" */'@/components/test'),
        meta: {
            // layout: 'default-layout',
            // login: true,
            // permission: 1,
            // classify: [1,1],
        }
    }
];
