export default { 
    state: () => ({
        user: {}
    }),
    mutations: {
        setUser(state, data) {
            state.user = data;
        },
    }
}