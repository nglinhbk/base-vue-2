import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const requireModule = require.context('../modules', true,  /\/.*\/store\/index\.js$/);
const modules = {};
requireModule.keys().forEach(filename => {
    const moduleName = filename
        .replace(/(\.\/|\.js)/g, '')
        .split('/')[0];
    modules[moduleName] = requireModule(filename).default || requireModule(filename);
    modules[moduleName].namespaced = true;
});


modules['general'] = {
    namespaced: true,
    state: () => ({
        layout_main: 'default-layout',
        redirect: null,
        dashboard: [],
    }),
    mutations: {
        setLayoutMain(state, data) {
            state.layout_main = data ? data : 'default-layout';
        },
        setRedirect(state, data){
            if(data === null){
                state.redirect = '';
            }else{
                try{
                    let redirect = JSON.parse(atob(data));
                    let url = '?';
                    for(const key in redirect){
                        url += key + '=' + redirect[key] + '&'
                    }
                    state.redirect = url.slice(0, -1);
                }catch(e){
                    state.redirect = '';
                }
            }
        },
        setDashboard(state, data) {
            state.dashboard = data;
        },
    },
}

export default new Vuex.Store({
    modules: modules,
})
