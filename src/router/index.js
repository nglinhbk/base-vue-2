import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);
import { getToken, messageAuthorization, messageNotPermission } from '@/config/functions';
import { information } from '@/repositories/user';

const key_access_token = process.env.VUE_APP_KEY_ACCESS_TOKEN;

const requireModule = require.context('../modules', true,  /\/.*\/router\/index\.js$/);
let moduleName = [];
let oneRoute = [];
requireModule.keys().forEach(filename => {
    oneRoute = requireModule(filename).default || requireModule(filename);
    for (const key in oneRoute) {
        moduleName.push(oneRoute[key]);
    }
})

const routes = [...moduleName];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

router.beforeEach(async (to, from, next) => {
    const redirect = to.query.redirect || null;
    $store.commit('general/setRedirect', redirect);
    let user = $store.state.user.user;
    let token = getToken(key_access_token);
    let login = to.meta.login;
    if(login === false){
        if(token) next({name: 'layout'});
    }else if(login === true){
        if(!token){
            messageAuthorization();
            next({name: 'login'});
        }else{
            if(Object.keys(user).length == 0){
                await getUser();
            }
            user = $store.state.user.user;
            let permission = to.meta.permission ? parseInt(to.meta.permission) : null;
            let classify = to.meta.classify ? to.meta.classify : [];
            classify.map((item, index) => {classify[index] = parseInt(item)})
            if(!permission && classify.length == 0){
                next();
            }else{
                if(classify.indexOf(user.classify_admin) !== -1){
                    if(user.permissions.indexOf(permission) !== -1){
                        next();
                        return;
                    } 
                }
                messageNotPermission();
                // check thêm default route là gì
                if(from.name != 'layout'){
                    next({name: 'layout'});
                }
            }
        }
    }
    next();
})

router.afterEach((to, from) => {
    let meta = to.meta;
    let layout = meta.layout;
    if(!layout) layout = 'default-layout';
    $store.commit('general/setLayoutMain', layout);
})

let getUser = async () => {
    await information().then(res => {
        let data = res.data;
        $store.commit('user/setUser', data.user);
        $store.commit('general/setDashboard', data.dashboard);
    }).catch(err => {});
}

export default router
