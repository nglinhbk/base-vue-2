export const withPrefixRouter = (prefix, routes) => 
    routes.map((route) => {
        route.path = prefix + route.path;
        return route;
});

export const getNameModule = (path) => {
    let arrayNameModule = path.split('/');
    return arrayNameModule[0] != '.' ? arrayNameModule[0] : arrayNameModule[1];
}

export const upperCaseFirstLetter = (string) => {
    return string.toString()[0].toUpperCase() + string.slice(1);
}