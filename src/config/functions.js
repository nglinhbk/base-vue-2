export const getToken = (nameToken) => {
    let dataToken = document.cookie.split(';');
    nameToken += '=';
    let token = '';
    for(var i = 0; i <dataToken.length; i++) {
        if(dataToken[i].includes(nameToken) == true){
            token = dataToken[i];
        }
    }
    if(token != ''){
        token = token.split('=')[1];
        return token;
    }else{
        return '';
    }
}

export const messageAuthorization = () => {
    Vue.prototype.$message({
        message: 'Vui lòng đăng nhập',
        type: 'warning',
    });
}
export const messageNotPermission = () => {
    Vue.prototype.$message({
        message: 'Bạn không có quyền vào trang này',
        type: 'warning',
    });
}