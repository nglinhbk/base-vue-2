import { getNameModule, upperCaseFirstLetter } from '@/config/global';

const requireModule = require.context('../modules', true,  /\/.*\/config\/window\.js$/);

let oneWindow = [];
let nameModule, keyWindow;
requireModule.keys().forEach(filename => {
    nameModule = getNameModule(filename);
    oneWindow = requireModule(filename).default || requireModule(filename);
    if(typeof oneWindow === 'object') {
        for (const key in oneWindow) {
            keyWindow = nameModule + upperCaseFirstLetter(key)
            window[keyWindow] = oneWindow[key];
        }
    }
})