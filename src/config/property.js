import { getNameModule, upperCaseFirstLetter } from '@/config/global';

const requireModule = require.context('../modules', true,  /\/.*\/config\/properties\.js$/);

let oneProperty = [];
let nameModule, keyProperty, nameProperty;
requireModule.keys().forEach(filename => {
    nameModule = getNameModule(filename);
    oneProperty = requireModule(filename).default || requireModule(filename);
    for (const key in oneProperty) {
        nameProperty = key[0] == '$' ? key.slice(1) : key;
        keyProperty = '$' + nameModule + upperCaseFirstLetter(nameProperty);
        Vue.prototype[keyProperty] = oneProperty[key];
    }
});